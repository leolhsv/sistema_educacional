<?php

namespace SON\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /*Pega o usuário no qual o professor está atribuído*/
    public function user() 
    {
        return $this->morphOne(User::class, 'userable');
    }
}
