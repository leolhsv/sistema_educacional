

<h1>Listagem usuários em PDF</h1>

<table>
	<thead>
		<tr>
			<td>ID</td>
			<td>Nome</td>
			<td>Email</td>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $user)

			<tr>
				<td>{{$user->id}}</td>
				<td>{{$user->name}}</td>
				<td>{{$user->email}}</td>
			</tr>

		@endforeach
	</tbody>
</table>

