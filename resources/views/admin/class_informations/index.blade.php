
@extends('layouts.app')

@section('content')
    <div class="container">
        {{-- {!!dd($class_informations->items())->striped(); !!} --}}
        <div class="row">
            <h3>Listagem de turmas</h3>
            {!! Button::primary(Icon::plus().' Novo Turma')->asLinkTo(route('admin.class_informations.create')) !!}       
        </div>  
        <div class="row">
            {!! 
                Table::withContents($class_informations->items())
                ->striped()
                ->callback('Ações', function($field, $model) {
                    $linkEdit   = route('admin.class_informations.edit',['user' => $model->id]);
                    $linkDelete = route('admin.class_informations.destroy',['user' => $model->id]);
                    $linkShow   = route('admin.class_informations.show',['user' => $model->id]);
                    return Button::warning(Icon::create('pencil').' Editar')->asLinkTo($linkEdit).'  '.
                           Button::danger(Icon::create('trash').' Deletar')->asLinkTo($linkDelete).'  '.
                           Button::success(Icon::create('eye-open').' Ver')->asLinkTo($linkShow);
                }) 
            !!}
        </div> 

        {!! $class_informations->links() !!}
    </div>
@endsection




