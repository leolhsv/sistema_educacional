@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <h3>Listagem de usuários</h3>
            {!! Button::primary(Icon::plus().' Novo Usuário')->asLinkTo(route('admin.users.create')) !!}       
        </div>  
        <div class="row">
            {!! 
                Table::withContents($users->items())->striped()
                                                    ->callback('Ações', function($field, $model){
                    $linkEdit   = route('admin.users.edit',['user' => $model->id]);
                    $linkDelete = route('admin.users.destroy',['user' => $model->id]);
                    $linkShow   = route('admin.users.show',['user' => $model->id]);
                    return Button::warning(Icon::create('pencil').' Editar')->asLinkTo($linkEdit).'  '.
                           Button::danger(Icon::create('trash').' Deletar')->asLinkTo($linkDelete).'  '.
                           Button::success(Icon::create('eye-open').' Ver')->asLinkTo($linkShow);
                }) 
            !!}
        </div> 

        {!! $users->links() !!}
    </div>
@endsection