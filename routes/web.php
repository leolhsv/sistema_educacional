<?php


Route::get('/', function () {
    return view('welcome');
});

Route::prefix('admin')->group(function (){
    Auth::routes();

    Route::group(['prefix' => 'users', 'as' => 'admin.users.'], function(){

        Route::name('settings.edit')->get('settings', 'Admin\UserSettingsController@edit');
        Route::name('settings.update')->put('update', 'Admin\UserSettingsController@update');        
    });


    Route::group([

        'namespace' => 'Admin\\',
        'as' => 'admin.',
        'middleware' => ['auth', 'can:admin']

    ], function (){

        Route::name('dashboard')->get('/dashboard', function () {
            return "Estou no Dashbaord";
        });   

        Route::group(['prefix' => 'users', 'as' => 'users.'], function (){
           Route::name('show_details')->get('show_details', 'UsersController@showDetails'); 
           Route::group(['prefix' => '/{user}/profile'], function() {
                Route::name('profile.edit')->get('', 'UserProfileController@edit');
                Route::name('profile.update')->put('', 'UserProfileController@update');
           });
        });

        Route::resource('users', 'UsersController');
        Route::resource('subjects', 'SubjectsController'); //disciplinas
        Route::resource('class_informations', 'ClassInformationsController'); //turmas



/***************************************************************************************************************/
        // Teste geração PDF com barryvdh-DOMpdf
        // Route::get('users-testepdf', function() {
        //     $pdf = \PDF::LoadView('user-teste-pdf', ['users' => \SON\Models\User::all()]);
        //     return $pdf->stream();

        //     // return view('user-teste-pdf', ['users' => \SON\Models\User::all()]);
        // }); 


        // Teste geração de PDF com snappy (wktohtmlpdf)
        Route::get('users-testepdf', function() {
            $pdf = \PDF::LoadView('user-teste-pdf', ['users' => \SON\Models\User::all()]);
            return $pdf->inline();
        });


    });

/*******************************************************************************************************************/
});

//Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');

// Testando biblioteca pdf
Route::get('/testepdf', function() {
    $pdf = \PDF::loadHTML('<h1>TestePDF</h1>');

    //Abre em uma nova aba do navegador
    // return $pdf->stream(); 

    //Faz o download do arquivo
    $pdf->save(storage_path('app\meupdf2.pdf'));
    return $pdf->download('meupdf.pdf'); 
    
});
//teste

// Route::get('/teste', function() {
//     // return DB::table('users')->get();

//     $nome = "Leonardo";
//     $sobreNome = "vales";
//     $idade = "26";

//     $dados = [

//         'NomeL' => $nome,
//         'sobreNomeL' => $sobreNome,
//         'idadeL' => $idade,

//     ];

//     foreach ($dados as $key => $dado) {

//         if ($key == 'idadeL') 
//             $idade_oficial = $dado;
//         else if ($key == 'sobreNomeL')
//             $sobreNome_oficial = $dado;

//     }

//     echo $idade_oficial;
//     echo $sobreNome_oficial;    

// });


